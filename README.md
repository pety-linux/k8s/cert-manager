# cert-manager
cert-manager related stuff can be found here.


[[_TOC_]]

## Installation
### via yaml
```
kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.8.0/cert-manager.yaml
```

### via helm chart
```
helm repo add jetstack https://charts.jetstack.io

helm repo update

helm install \
  cert-manager jetstack/cert-manager \
  --namespace cert-manager \
  --create-namespace \
  --version v1.11.0 \
  --set installCRDs=true

```

### Check Installation

```
# satus of cert-manager
root@infra1:~# kubectl cert-manager check api
The cert-manager API is ready
root@infra1:~#


# CRDs
root@infra1:~# kubectl get crd |grep issuer
clusterissuers.cert-manager.io        2023-04-06T03:48:22Z
issuers.cert-manager.io               2023-04-06T03:48:23Z
```

### Create cert-manager issuers
```
# Staging
root@infra1:~# cat issuer-staging.yaml
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: letsencrypt-staging
spec:
  acme:
    server: https://acme-staging-v02.api.letsencrypt.org/directory
    email: pety.barczi@gmail.com
    privateKeySecretRef:
      name: letsencrypt-staging
    solvers:
      - http01:
          ingress:
            class: nginx


# Production
root@infra1:~# cat issuer-prod.yaml
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: letsencrypt-production
spec:
  acme:
    server: https://acme-v02.api.letsencrypt.org/directory
    email: pety.barczi@gmail.com
    privateKeySecretRef:
      name: letsencrypt-production
    solvers:
      - http01:
          ingress:
            class: nginx


# Verify
root@infra1:~# kubectl get clusterissuer
NAME                     READY   AGE
letsencrypt-production   True    4h29m
letsencrypt-staging      True    5h6m
```

## Deploy app using cert-manager

Prerequisities:
- nginx-ingress controller

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: wordpress
spec:
  replicas: 1
  selector:
    matchLabels:
      app: wordpress
  template:
    metadata:
      labels:
        app: wordpress
    spec:
      containers:
        - name: wordpress
          image: petyb/rocky:2
          ports:
            - containerPort: 80
---
apiVersion: v1
kind: Service
metadata:
  name: wordpress
spec:
  selector:
    app: wordpress
  ports:
    - protocol: TCP
      port: 80
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: wordpress
  annotations:
    cert-manager.io/cluster-issuer: letsencrypt-production
    kubernetes.io/ingress.class: "nginx"
spec:
  rules:
    - host: app.onmetal.sk
      http:
        paths:
          - path: /
            pathType: Prefix
            backend:
              service:
                name: wordpress
                port:
                  number: 80
  tls:
    - hosts:
      - app.onmetal.sk
      secretName: letsencrypt-production
```
NOTE: don't forget to use proper `secretName`

## Check certs
Ingress status whether cooperates with cert-manager properly:
```
root@infra1:~# kubectl describe ingress wordpress
Name:             wordpress
Labels:           <none>
Namespace:        default
Address:          192.168.1.54
Ingress Class:    <none>
Default backend:  <default>
TLS:
  letsencrypt-production terminates app.onmetal.sk
Rules:
  Host            Path  Backends
  ----            ----  --------
  app.onmetal.sk
                  /   wordpress:80 (10.42.1.10:80)
Annotations:      cert-manager.io/cluster-issuer: letsencrypt-production
                  kubernetes.io/ingress.class: nginx
Events:           <none>
```

Generates secrets:
```
root@infra1:~# kubectl get secret
NAME                     TYPE                DATA   AGE
letsencrypt-production   kubernetes.io/tls   2      4h24m
```

Generated certs:
```
root@infra1:~# kubectl get certificates
NAME                     READY   SECRET                   AGE
letsencrypt-production   True    letsencrypt-production   4h23m


```

## Access app 
Verify that TLS works
```
https://app.onmetal.sk
```
